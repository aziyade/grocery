package domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//public class domain.Panier {   // n'est pas une extension de produit


public class Panier {


    // créer une méthode pour afficher l'heure de finalisation du panier
//......................................................................................

    // méthode : getter de l'objet createdAt
    public Instant getCreatedAt() {
        return createdAt;
    }
// objet createdAt qui retourne la methode now afin d'obtenir l'heure actuelle
    private Instant createdAt = Instant.now();
//......................................................................................



//creer une liste de produits dans le panier
//..............................................................................................
    //methode: getter de  productsInBasket
    public ArrayList<Product> getProductsInBasket() {
        return productsInBasket;
    }

    //les produits dans le panier sont une nouvelle liste sous forme de tableau (Arraylist)
    private ArrayList<Product> productsInBasket = new ArrayList<>(30);  // la capacité max est de 10 élements, ici elle est poussée à 30
//...........................................................................................



//............methode ajouter................................................................
    public void ajouter(Product products) {
        productsInBasket.add(products);
    }
//...........................................................................................



//methode: calcul total du panier
//.....................................................................................................
    public int getTotal() {  //ArrayList<Product> s'écrit à l'endroit de string, int
        // je cree la fct getTotal et j'appelle ses parametre: je vais dans l'objet panier de la classe panier
        int total = 0;
        for (int i = 0; i < this.productsInBasket.size(); i++) {
// parcourir le panier (i++), panier.size permet de prendre en compte la taille du panier = liste de products, chercher dedans le prix de chaque products et faire la somme
            total += this.productsInBasket.get(i).getPrice();
        }
        return total;
    }

//..................................................................................................


//......methode remove...................................................................................

    public void remove(Product product) {
        productsInBasket.remove(product);
    }
//................................................................................................



//........methode finaliser.......................................................................
    public Order finalizeBasket() {
        Order order = new Order();


        productsInBasket.clear();
        return order;
    }
//................................................................................................

}
