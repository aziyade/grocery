package domain;

public class Product {   // decrit le produit
//...............................................................................................
// attribut de product
    private String name;
    private int price;

    public Product(String name, int price)
            //throws Exception
    { // la fct Product à comme
        // attributs un name et un price et comme méthode la possibilité de jeter/ throw l'exception
        this.name = name ;


        this.price = price;
        if(this.price < 0) {
            throw new RuntimeException();
        }




    }

    @Override
    public String toString() {
        return "domain.Product{" +
                "name='" + name + '\'' + " ,price="+ price +
                '}';
    }

    public String getName() {  return name; }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
