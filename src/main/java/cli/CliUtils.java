package cli;

import domain.Panier;
import domain.Product;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class CliUtils {

    public static Panier panier = new Panier(); // 1er mot = type de la variable

    public static List<Product> products = List.of(
            new Product("fraises", 5),
            new Product("ananas", 2),
            new Product("pommes", 1)
    );

    public static int getUserChoice(Scanner scanner) {

        return Integer.parseInt(scanner.nextLine());
    }


    // recuperer un produit par son nom
    public static Product getProductFromName(String productName) {
        for (Product product : products) {
            if (productName.equals(product.getName())) {
                return product;
            }
        }
        return null;
    }


}
