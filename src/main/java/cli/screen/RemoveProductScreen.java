package cli.screen;

import cli.CliUtils;
import domain.Product;

import java.util.ArrayList;
import java.util.Scanner;

public class RemoveProductScreen  implements Screen {

    @Override
    public void displayScreen() {
    //public static void removeProductMenu() {


//proposer un choix de numéro (correspondant à l'index -1)

        ArrayList<Product> productsInBasket = CliUtils.panier.getProductsInBasket(); // je peux faire directement
        //   for (int i = 0; i < panier.getproductsInBasket().size(); i++) {
        for (int i = 0; i < productsInBasket.size(); i++) { // itération sur une liste  = parcourir la liste :
            // for ( Product p : products)
            // afficher la liste = le panier
            // voulez vous enlever un product? Y/N

            System.out.println((i + 1) + ". " + productsInBasket.get(i).getName() + " " + productsInBasket.get(i).getPrice() + "€");

        }
        System.out.println("voulez vous enlever un produit? Y/N");

        // TODO move scanner outside Panier class
        Scanner scanner = new Scanner(System.in);

        String reponse = scanner.nextLine();
        // si Y, quel numero?*
        if (reponse.equalsIgnoreCase("Y")) { // .equals par default,
            // ici ont met .equalsIgnoreCase pour ignorer majuscule ou min
            System.out.println("quel numéro d' article?");
            // supprimer l'article

            int choosedNumber = scanner.nextInt();

            // supprimer l'article à l'index numeroProduit - 1
            CliUtils.panier.remove(productsInBasket.get(choosedNumber - 1));
            //productsInBasket.remove(choosedNumber - 1);

        }

        // si N afficher panier
        else {
            System.out.println(productsInBasket + "finalisez votre commande");
        }


    }
}
