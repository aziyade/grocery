package cli.screen;

import cli.CliUtils;
import domain.Product;

import java.util.Scanner;

public class AddProductScreen  implements Screen  {

    @Override
    public void displayScreen() {
/* public void ajouter(Product products) {
            productsInBasket.add(products);
        }*/

        System.out.println("veuillez ajouter un produit (fraises, ananas  ou pommes ?)");
        Scanner input = new Scanner(System.in);
        String productName = input.nextLine();

        Product product = CliUtils.getProductFromName(productName);

        System.out.println("le produit " + productName + " vient d'etre ajouté");

        // int i = 0;
        CliUtils.panier.ajouter(product);
    }
}
