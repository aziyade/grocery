package cli.screen;

import cli.CliUtils;
import domain.Order;
import domain.Product;

public class FinalyseMenuScreen implements Screen  {

    @Override
    public void displayScreen() {  // je fais une methode pour afficher l'ecran ( IDEM DANS TT LES CLASSES SCREEN)
   // public static void finalyseMenu() { // cette methode n'est plus utile depuis que je suis passée en mode ecran
        // finaliser le panier

        Order order = CliUtils.panier.finalizeBasket();


        System.out.println("finaliser le panier crée le :" + CliUtils.panier.getCreatedAt());

        for (Product product : CliUtils.panier.getProductsInBasket()) {   // objet.methode pour appeller les product in basket
            // pour each product ds panier afficher produit
            System.out.println(product.toString());
        }

        System.out.println("total:" + CliUtils.panier.getTotal());

    }
}
