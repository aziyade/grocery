package cli.screen;

import cli.CliUtils;

import java.util.Scanner;

public class MainMenuScreen implements Screen{

    @Override
    public void displayScreen() {

  //  public static void mainMenu() {
        int actionChoose = 0;

        do {
            System.out.println("please, choose a action : 1(ajouter produit) 2(supprimer produit) or 3 (finaliser le panier)");
            Scanner scanner = new Scanner(System.in);
            actionChoose = CliUtils.getUserChoice(scanner);


            if (actionChoose == 1) {
                //ajouter au panier
                AddProductScreen addProductScreen = new AddProductScreen(); // pour methode non statique
                addProductScreen.displayScreen();
             //   AddProductScreen.addProductMenu();
                // ici l'ancienne version, methode statique
                // alt + entrée me crée une méthode plus bas :    private static void removeProductMenu()
            }
            if (actionChoose == 2) {
                // remove du panier
                RemoveProductScreen removeProductScreen = new RemoveProductScreen();
                removeProductScreen.displayScreen();
                //RemoveProductScreen.removeProductMenu();
            }

        } while (actionChoose != 3);
        FinalyseMenuScreen finalyseMenuScreen = new FinalyseMenuScreen();
        finalyseMenuScreen.displayScreen();
        //FinalyseMenuScreen.finalyseMenu();
    }



    public static void main(String[] args) {

        MainMenuScreen mainMenuScreen = new MainMenuScreen();
        mainMenuScreen.displayScreen(); // selectionner un gros bout de code (ici la boucle do while), refactor puis extract method


    }


}
