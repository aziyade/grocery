package domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PanierTest {

    @Test
    void addManyProducts(){

        Panier panier = new Panier();
        Product product1 = new Product("poire", 3);

        panier.ajouter(product1);
        panier.ajouter(product1);

        Assertions.assertEquals(6,panier.getTotal()); // 3 et panier.getTotal() sont compares
    }

    @Test
    void deleteProduct() {
        Panier panier = new Panier();
        Product product1 = new Product("poire", 3);

        panier.ajouter(product1);
        panier.remove(product1);

        Assertions.assertEquals(0, panier.getTotal());
    }
}
